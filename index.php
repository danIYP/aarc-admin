<?php 


require_once('inc/header.php');
if ($page_id) {
	if ($is_enabled || $is_admin) {
		echo '<input type="hidden" id="page_code" name="page_code" value="' . $page_code . '" />';
		?>
		<!-- start: PAGE TITLE -->
		<section id="page-title">
			<div class="row">
				<div class="col-sm-8">
					<h1 class="mainTitle"><?php echo $title; ?></h1>
					<?php echo ((strlen($headline))?'<span class="mainDescription">' . $headline . '</span>':''); ?>
				</div>
				<?php echo $bc; ?>
			</div>
		</section>
		<!-- end: PAGE TITLE -->
		<!-- start: BASIC EXAMPLE -->
		<div class="container-fluid container-fullw bg-white">
			<div class="row">
				<div class="col-md-12">
					<div class="panel1 panel1-white">
						<div class="panel1-body content-body">
							<?php
							//echo $content_5."<br>";
							echo iif(!$is_enabled, '<div class="text-center"><span class="alert alert-info"><i class="fa fa-exclamation-triangle"></i> Note: This page is not active and cannot be viewed by agents.</span><hr /></div>');
					if ($page_type_id == 5) { // blog
						try {
							$file = $content_5;
					// Load specified XML file or report failure
							@$xml = simplexml_load_file($file) or die ("Unable to load XML file!");
					// Load blog entries
							$result = $xml->xpath('//content:encoded');
							$xml =  $xml->channel->item;
					// Run loop for the number of available entries
							$i = 0;

							foreach( $xml as $row ) {
					if(isset($_GET['jj'])){
								echo "<pre>";
								print_r($xml);
								echo "</pre>";
							}

					//var_dump($row);
					// Load the entry publish time
								
									
								$dtime = date("D jS M, Y", strtotime($row->pubDate));
					// Load the link of each blog entry
								$titlelink = $row->link[4]['href'];
					// Load the text for Comment and comment counts
								$comments = $row->link[1]['href'];
								$comm = $row->link[1]['title'][0];
								
								/* Display the contents (use your own imaginations here =).) */
								echo '<div class="panel panel-white">
								<div class="panel-heading border-light">
									<h2 class="panel-title">';
							//echo '<a href="'. $titlelink . '" target="_blank">' . $row->title . '</a>';
										echo $row->title;
										echo '	</h2>
									</div>';
									echo '<div class="panel-body content-text">';
									echo $result[$i];
							// Display number of comments
									echo 'Comments: ' . $comm . '</div>
									<div class="panel-footer">Published on: ' . $dtime . '</div></div>';
									$i++;
								}
							} catch (Exception $e) {
								echo 'Oops';
							}
						}
						else {
							echo '<div class="content-text">' . $content . '</div></span></span></span>';
							
							if ($page_type_id == 3 and strlen($content_3)) {
								$o = json_decode($content_3, true);
								echo '<div class="row">';
								for ($i = 0; $i<100; $i++) {
									if (isset($o['title_' . $i])) {
										echo '
										<div class="col-sm-6">
											<div class="panel panel-white">
												<div class="panel-heading border-light">' . $o['title_' . $i] . '</div>
												<div class="panel-body video-container">' . $o['code_' . $i] . '</div>
											</div>
										</div>
										';
									}
									else {
										break;
									}
								}
								echo '</div>';
								
							}
							
						}
						
						echo
						iif(strlen($content) && !$is_enabled, '<div class="text-center"><hr /><span class="alert alert-info"><i class="fa fa-exclamation-triangle"></i> Note: This page is not active and cannot be viewed by agents.</span></div>');
						
						$rs_read = getRs("SELECT member_page_read_id, date_created FROM member_page_read WHERE " . is_enabled() . " AND page_id = ? AND member_id = ?", array($page_id, $_SESSION['member_id']));
						echo '<div class="text-right margin-top-10"><hr />
						';
									//echo getNextPage($page_id);
						if ($row_read = getRow($rs_read)) {
							echo '<span id="page_read_status"><span class="text-success"><i class="ti-check"></i> Marked as read on ' . dateTime($row_read['date_created']) . '</span></span>
							<button type="button" class="btn btn-warning btn-sm btn-page-unread"><i class="ti-close"></i> Mark as unread</button>';
						}
						else {
							echo '<span id="page_read_status"></span><button type="button" class="btn btn-success btn-page-read"><i class="ti-check"></i> Mark as read</button>';
						}
						echo '
					</div>';
					
					?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php
} else {
	?>
	<div class="container">
		<div class="row">
			<div class="col-sm-12 page-error" style="padding-top:50px">
				<h1 class="error-number1 text-azure">
					Page Not Found
				</h1>
				<div class="error-details col-sm-6 col-sm-offset-3">
					<h3>Sorry <?php echo $_SESSION['member_first_name']; ?>, the page you were looking for could not be found.</h3>
					<p>
						It may be temporarily unavailable, moved or no longer exist.
						<br>
						Check the URL you entered for any mistakes and try again.
						<br>
						<a href="index.php" class="btn btn-red btn-return">
							Return home
						</a>
						<br>
						Still no luck?
						<br>
						Search for whatever is missing, or take a look around the rest of our site.
					</p>
					<form action="search.php" method="get">
						<div class="input-group">
							<input type="text" name="kw" placeholder="keyword..." size="16" class="form-control">
							<span class="input-group-btn">
								<button class="btn btn-azure">
									Search
								</button> </span>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<?php
	}
} else if ($announcement_id) {
	?>					<!-- start: PAGE TITLE -->
	<section id="page-title">
		<div class="row">
			<div class="col-sm-8">
				<h1 class="mainTitle"><i class="fa fa-bullhorn" style="font-size:30px"></i> <?php echo $announcement_name; ?></h1>
				<span class="mainDescription">Posted on: <?php echo dateTime($date_modified);?></span>
			</div>
			<ol class="breadcrumb">
				<li><a href="index.php">Message Board</a></li>
				<li><?php echo $announcement_type_name; ?></li>
				<li class="active"><?php echo $announcement_name; ?></li>
			</ol>
		</div>
	</section>
	<!-- end: PAGE TITLE -->
	<!-- start: BASIC EXAMPLE -->
	<div class="container-fluid container-fullw bg-white">
		<div class="row">
			<div class="col-md-12">
				<?php
				echo $description;
				?>
			</div>
		</div>
	</div>
	<?php
} else {
	$is_home = true;
	$rs = getRs("SELECT a.*, t.announcement_type_name FROM announcement a INNER JOIN announcement_type t ON t.announcement_type_id = a.announcement_type_id WHERE " . is_enabled('a') . " ORDER BY a.sort, a.announcement_id")
	
	?>
	<section id="page-title" class="padding-top-15 padding-bottom-15">
		<div class="row">
			<div class="col-sm-12">
				<h1 class="mainTitle"><i class="ti-home" style="font-size:30px"></i> Welcome to your Dashboard</h1>
			</div>
		</div>
	</section>
	
	<div class="container-fluid container-fullw bg-white">
		
		<?php if ($is_admin) { ?>
		
		<form action="members-search.php" method="get">
			<div class="input-group well">
				<input type="text" name="kw" class="form-control" placeholder="Enter agent's name, ID, phone number, or email address&hellip;">
				<span class="input-group-btn">
					<button class="btn btn-primary" type="submit">
						<i class="fa fa-search"></i> Find Agent
					</button> </span>
				</div>
			</form>
			<?php } ?>
			
			
			<?php if (strlen($setting_notice)) { ?>
			<div class="alert alert-block alert-warning fade in notice-block">
				<button type="button" class="close" data-dismiss="alert" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
				<?php echo $setting_notice; ?>
			</div>
			<?php } ?>
			
			<?php if ($is_admin) { ?>
			<?php if (strlen($setting_notice)) { ?>
			<a href="" class="btn btn-primary btn-edit-notice btn-sm margin-bottom-20"><i class="fa fa-pencil"></i> Edit announcement</a>
			<?php } else { ?>
			<a href="" class="btn btn-warning btn-edit-notice btn-sm margin-bottom-20"><i class="fa fa-plus"></i> Add site-wide announcement</a>
			<?php } ?>
			<form action="" id="f_notice" method="post" role="form" class="hide">
				<div class="form-group">
					<textarea id="setting_notice" name="setting_notice" class="ckeditor form-control" cols="10" rows="10" placeholder="Type your notice. It will be displayed on every member's dashboard&hellip"><?php echo $setting_notice; ?></textarea>
				</div>
				<div class="form-group">
					<div class="row">
						<div class="col-sm-6">
							<button type="submit" class="btn btn-primary">Save</button> <button type="button" class="btn btn-danger btn-del-notice">Clear</button>
						</div>
						<div class="col-sm-6 text-right">
							<div id="notice_status"></div>
						</div>
					</div>
				</div>
			</form>
			<?php } ?>
			
			
			
			<div class="row">
				<?php
				if ($is_admin) {
					$rs_s = getRs("SELECT SUM(CASE WHEN member_status_id = 1 THEN 1 ELSE 0 END) AS active_members, SUM(CASE WHEN member_status_id = 1 THEN 0 ELSE 1 END) AS inactive_members FROM member WHERE " . is_enabled() . " AND member_access_level_id = 1");
					if ($row_s = getRow($rs_s)) {
						$active_members = $row_s['active_members'];
						$inactive_members = $row_s['inactive_members'];
						?>
						<div class="col-sm-12">
							<div class="panel panel-white no-radius">
								<div class="panel-heading border-light">
									<h4 class="panel-title"> Agent <b>Retention Report</b> (Yearly)</h4>
								</div>
								<div class="panel-body">
						<!--
						<h3 class="inline-block no-margin"><?php echo number_format($active_members); ?></h3> Total Active Agents
						<div class="progress progress-xs no-radius">
							<div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="<?php echo (($active_members * 100)/($active_members + $inactive_members)); ?>" aria-valuemin="0" aria-valuemax="100" style="width: <?php echo (($active_members * 100)/($active_members + $inactive_members)); ?>%;">
								<span class="sr-only"> <?php echo number_format(($active_members * 100)/($active_members + $inactive_members)); ?>% Complete</span>
							</div>
						</div>-->
						<div class="margin-top-10">
							<div class="height-300">
								<canvas id="chart2" class="full-width"></canvas>
								<div class="inline pull-left legend-xs">
									<div id="chart2Legend" class="chart-legend"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-12" style="display:none">
				<div class="panel panel-white no-radius">
					<div class="panel-heading border-light">
						<h4 class="panel-title"> Agent <b>Retention Report</b> (Monthly)</h4>
					</div>
					<div class="panel-body">
						<div class="margin-top-10">
							<div class="height-300">
								<canvas id="chart3" class="full-width"></canvas>
								<div class="inline pull-left legend-xs">
									<div id="chart3Legend" class="chart-legend"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<!-- start: FEATURED BOX LINKS
		-->
		
		<div class="col-md-12" style="display:none">
			<div class="panel panel-white no-radius" id="visits">
				<div class="panel-heading border-light">
					<h4 class="panel-title"> Agent <b>Active Report </b>(Monthly)  </h4>
				</div>
				<div collapse="visits" class="panel-wrapper">
					<div class="panel-body">
						<div class="height-350">
							<canvas id="chart4" class="full-width"></canvas>
							<div class="margin-top-20">
								<div class="inline pull-left">
									<div id="chart4Legend" class="chart-legend"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="col-md-12 hide">
			<div class="panel panel-white no-radius" id="visits">
				<div class="panel-heading border-light">
					<h4 class="panel-title"> Client <b>Duration</b> Report </h4>
				</div>
				<div collapse="visits" class="panel-wrapper">
					<div class="panel-body">
						<div class="height-350">
							<canvas id="chart1" class="full-width"></canvas>
							<div class="margin-top-20">
								<div class="inline pull-left">
									<div id="chart1Legend" class="chart-legend"></div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<?php
	}
}
function getReads($page_id, $f = 'num_reads') {
	$num_reads = 0;
	$rs = getRs("SELECT p.page_id, (CASE WHEN r.member_page_read_id > 0 THEN 1 ELSE 0 END) AS num_reads, 1 AS num_pages FROM page p LEFT JOIN member_page_read r ON r.page_id = p.page_id AND r.member_id = ? AND " . is_enabled('r') . " WHERE p.parent_page_id = ? AND " . is_enabled('p'), array($_SESSION['member_id'], $page_id));
	foreach($rs as $row) {
		$num_reads += $row[$f];
		$num_reads += getReads($row['page_id'], $f);
	}
	return $num_reads;
}
?>
<?php


$rs_r = getRs("SELECT p.page_id, p.page_name, (CASE WHEN r.member_page_read_id > 0 THEN 1 ELSE 0 END) AS num_reads FROM page p LEFT JOIN member_page_read r ON r.page_id = p.page_id AND r.member_id = ? AND " . is_enabled('r') . " WHERE p.page_type_id NOT IN (4,5) AND p.parent_page_id = 0 AND " . is_enabled('p') . " ORDER BY p.sort, p.page_id", array($_SESSION['member_id']));
foreach($rs_r as $row_r) {
	
	$rs_c = getRs("SELECT page_id FROM page WHERE parent_page_id = {$row_r['page_id']} AND " . is_enabled() . " ORDER BY sort, page_id LIMIT 1");
	if ($row_c = getRow($rs_c)) {
		$next_page_id = $row_c['page_id'];
	}
	else {
		$next_page_id = $row_r['page_id'];
	}
	
	$p = ((getReads($row_r['page_id']) + $row_r['num_reads'])/(getReads($row_r['page_id'], 'num_pages') + 1)*100);
	echo '<div class="col-sm-4">
	<div class="panel panel-white no-radius text-center">
		<div class="panel-body">
			<div class="text-center">
				<span class="chart" data-percent="' . $p . '">
					<span class="percent"></span>
					<span class="footnote"></span>
				</span>
			</div>
			<h2 class="StepTitle">'. $row_r['page_name'] . '</h2>';
			if ($p < 100) {
				echo '
				<p class="text-small">
					Next in this series:
				</p>
				<p class="links cl-effect-1">
					' . getNextPage($next_page_id, '', true, true) . '
				</p>';
			}
			echo '
		</div>
	</div>
</div>';
}
?>
</div>
</div>
<!-- end: FEATURED BOX LINKS -->

<!-- start: FEATURED BOX LINKS -->
<div class="container-fluid container-fullw bg-white">
		<!--
									<div class="row">
											<div class="col-sm-3"></div>
											<div class="col-sm-6">
													<div class="panel panel-white no-radius text-center">
															<div class="panel-body">
																	<span class="fa-stack fa-2x"> <i class="fa fa-square fa-stack-2x text-primary"></i> <i class="fa fa-bullhorn fa-stack-1x fa-inverse"></i> </span>
																	<h2 class="StepTitle">Message Board</h2>
																	<div class="text-left">
							<ul class="timeline-xs margin-top-15 margin-bottom-15">
								<?php
																				foreach($rs as $row) {
								echo '<li class="timeline-item danger"><div class="margin-left-15"><a href="index.php?a=' . $row['announcement_id'] . '">' . $row['announcement_name'] . '</a></div></li>';
																				}
								?>
							</ul>
						</div>
					</div>
				</div>
			</div>
			<div class="col-sm-3"></div>
		</div>
	-->
</div>
<!-- end: FEATURED BOX LINKS -->
<?php
}
require_once('inc/footer.php'); ?>